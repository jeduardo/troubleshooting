# Java TCP TLS Client

## Build instructions

Just run *mvn package*.

## Usage instructions

After building the package, execute the following command:

```ShellSession
$ java -jar target/java-tcp-tls-client-1.0.0.one-jar.jar <remote server> <remote port>
```

For example:

```ShellSession
$ java -jar target/java-tcp-tls-client-1.0.0.one-jar.jar www.google.com 443
Connecting to www.google.com on port 443 using TLS... successful!
```

On certificate validation failures, a message will be issued informing that the validation has failed:

```ShellSession
$ java -jar target/java-tcp-tls-client-1.0.0.one-jar.jar lb.prod.zp.deposit 8543
Connecting to lb.prod.zp.deposit on port 8543 using TLS... failed for reason: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
``` 

This means that the certificate used by this server is not signed by an authority recognized by the JVM as valid.
package com.depositsolutions.infrastructure.tls;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;

public class TLSConnectionClient {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Server and port must be specified!");
            System.exit(1);
        }
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        System.out.print(String.format("Connecting to %s on port %d using TLS...", host, port));
        try {
            SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory
                    .getDefault();
            SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(
                    host, port);
            sslsocket.startHandshake();
            System.out.println(" successful!");
            sslsocket.close();
        } catch (IOException ex) {
            System.out.println(String.format(" failed for reason: %s", ex.getMessage()));
        }
    }
}

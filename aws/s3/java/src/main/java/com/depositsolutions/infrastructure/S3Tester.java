package com.depositsolutions.infrastructure;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class S3Tester {
    private final static Logger LOGGER = LoggerFactory.getLogger(S3Tester.class);

    public static void main(String[] args) throws IOException {
        if (args.length < 4) {
            LOGGER.error("Usage: AWS_ACCESS_KEY AWS_SECRET_KEY AWS_REGION AWS_BUCKET_NAME TEST_FILE_NAME");
            System.exit(1);
        }
        String awsAccessKey = args[0];
        String awsSecretKey = args[1];
        String awsRegion = args[2];
        String awsBucket = args[3];
        String testFile = args[4];
        String uploadPath = "s3-test.data";
        String downloadPath = "s3-download.data";
        AWSCredentials creds = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
        LOGGER.info("Starting test run for S3 access...");

        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(creds))
                .withRegion(awsRegion)
                .build();
        LOGGER.info(String.format("LIST TEST: LIST contents of bucket %s on region %s", awsBucket, awsRegion));
        try {
            ObjectListing listing = s3.listObjects(awsBucket);
            for (S3ObjectSummary summary : listing.getObjectSummaries()) {
                LOGGER.info(summary.getKey());
            }
            LOGGER.info("LIST TEST: SUCCESS");
        } catch (AmazonS3Exception ex ) {
            LOGGER.error(String.format("LIST TEST: FAIL: %s", ex.getMessage()));
        }

        LOGGER.info(String.format("UPLOAD TEST: UPLOAD %s of bucket %s on region %s as %s", testFile, awsBucket, awsRegion, uploadPath));
        try {
            s3.putObject(awsBucket, uploadPath, new File(testFile));
            LOGGER.info("UPLOAD TEST: SUCCESS");
        } catch (AmazonS3Exception ex) {
            LOGGER.error(String.format("UPLOAD TEST: FAIL: %s", ex.getMessage()));
        }

        LOGGER.info(String.format("DOWNLOAD TEST: DOWNLOAD %s of bucket %s on region %s as %s", uploadPath, awsBucket, awsRegion, downloadPath));
        try {
            S3Object s3obj = s3.getObject(awsBucket, uploadPath);
            S3ObjectInputStream is = s3obj.getObjectContent();
            FileUtils.copyInputStreamToFile(is, new File(downloadPath));
            LOGGER.info("DOWNLOAD TEST: SUCCESS");
        } catch (AmazonS3Exception ex ) {
            LOGGER.error(String.format("DOWNLOAD TEST: FAIL: %s", ex.getMessage()));
        }

        LOGGER.info(String.format("DOWNLOAD TEST: DELETE %s of bucket %s on region %s", uploadPath, awsBucket, awsRegion));
        try {
           s3.deleteObject(awsBucket, uploadPath);
            LOGGER.info("DOWNLOAD TEST: SUCCESS");
        } catch (AmazonS3Exception ex ) {
            LOGGER.error(String.format("DOWNLOAD TEST: FAIL: Unable to delete uploaded object from bucket: %s", ex.getMessage()));
        }

        LOGGER.info("Test run completed");
    }
}

# Java tester for AWS S3

This software will try to connect to Amazon S3, list the contents of a given bucket and to upload a file to it.

## Build

The application can be built with `mvn package`.

## Usage

```shell
java -jar target/s3-test-1.0.0.one-jar.jar $AWS_ACCESS_KEY $AWS_SECRET_KEY $AWS_REGION_CODE $AWS_BUCKET_NAME $TEST_FILE_PATH
```

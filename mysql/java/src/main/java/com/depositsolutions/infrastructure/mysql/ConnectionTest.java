package com.depositsolutions.infrastructure.mysql;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ConnectionTest {
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionTest.class.getName());

    public static void main(String[] args) throws SQLException {
        String jdbcUrl = null;
        String statement = "select concat(now(), ' - ', version()) as result";

        if (args.length < 1) {
            LOG.error("JDBC URL not specified");
            System.exit(1);
        }
        if (args.length < 2) {
            LOG.error("No statement passed as parameter, using default statement");
        } else {
            statement = args[1];
        }
        jdbcUrl = args[0];
        Connection conn = DriverManager.getConnection(jdbcUrl);
        LOG.info(String.format("Connection fetched: %s", conn.toString()));
        LOG.info(String.format("Running statement: %s", statement));
        PreparedStatement pst = conn.prepareStatement(statement);
        ResultSet rs = pst.executeQuery();
        boolean columnsOutputted = false;
        int columnNumber = -1;
        while (rs.next()) {
            List<String> columns = new ArrayList<>();
            List<String> separators = new ArrayList<>();
            if (!columnsOutputted) {
                ResultSetMetaData rsmd = rs.getMetaData();
                columnNumber = rsmd.getColumnCount() + 1;
                for (int i = 1; i < columnNumber; i++) {
                    columns.add(rsmd.getColumnName(i));
                    separators.add(StringUtils.repeat("-", rsmd.getColumnName(i).length()));
                }
                columnsOutputted = true;
                LOG.info(String.join("|", columns));
                LOG.info(String.join("|", separators));
            }
            columns.clear();
            for (int i = 1; i < columnNumber; i++) {
                columns.add(rs.getString(i));
            }
            LOG.info(String.join("|", columns));
        }
        LOG.info("Query executed successfully");
    }
}
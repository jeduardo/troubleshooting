# MySQL Java Client

This is a simple utility using basic JDBC libraries that can be used to ensure that the connection from a server to a MariaDB cluster is working as expected.

It executes a simple query looking for the current date and remote server version and outputs this to the console.

It can be packaged as a runnable JAR, having the JVM as its only dependency.

## Build instructions

Just run *mvn package*.

## Usage instructions

After building the package, execute the following command:

```ShellSession
java -jar ./target/java-mysql-client-1.0.0.one-jar.jar 'jdbc:mysql://server:port/database?user=myuser&password=mypassword'
```
It is also possible to specify a test statement, as below:

```ShellSession
java -jar ./target/java-mysql-client-1.0.0.one-jar.jar 'jdbc:mysql://server:port/database?user=myuser&password=mypassword' "select 1 + 1 as result"
```

## TODOs

* No error handling code whatsoever, it verifies only the happy flow.

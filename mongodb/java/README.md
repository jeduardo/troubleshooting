# Java Mongo Client

## Build instructions

Just run *mvn package*.

## Usage instructions

After building the package, execute the following command:

```ShellSession
java -jar ./target/java-mongo-client-1.0.0.one-jar.jar 'mongodb://user:pass@mongo1.dev.infra.deposit:27017,mongo2.dev.infra.deposit:27017/db?replicaSet=replicaname&authSource=admin&ssl=true'
```

## TODOs

* No error handling code whatsoever, it verifies only the happy flow.

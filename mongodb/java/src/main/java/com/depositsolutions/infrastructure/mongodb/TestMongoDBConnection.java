package com.depositsolutions.infrastructure.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.StringJoiner;

public class TestMongoDBConnection {
    private final static Logger LOGGER = LoggerFactory.getLogger(TestMongoDBConnection.class);

    public static void main(String[] args) {
        if (args.length < 1) {
            LOGGER.error("Usage: $MONGODB_URL");
            System.exit(1);
        }
        MongoClientURI uri = new MongoClientURI(args[0]);
        String database = uri.getDatabase();
        LOGGER.info("Starting test run for MongoDB access...");
        MongoClient client = new MongoClient(uri);
        MongoDatabase db = client.getDatabase(database);
        StringJoiner joiner = new StringJoiner(",");
        for (String dbName : db.listCollectionNames()) {
            joiner.add(dbName);
        }
        LOGGER.info(String.format("Collections in the database: %s", joiner.toString()));
        LOGGER.info("Test run completed");
    }
}
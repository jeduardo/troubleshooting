# Java tester for RabbitMQ

This software will connect to a remote RabbitMQ server, post a message to a 
durable queue and then consume this message right after posting it.

## Build

The application can be built with `mvn package`.

## Usage

```shell
java -jar target/rabbit-test-1.0.0.one-jar.jar amqps://<user>:<password>@<server>:<port>/<vhost> <queue>
```
